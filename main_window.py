from PyQt5.QtWidgets import QMainWindow, QMessageBox

from connect_dialog import ConnectDialog
from db import db
from table_window import TableWindow
from ui_py.main_window import Ui_MainWindow


class MainWindow(QMainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.setFixedSize(self.size())
        self.table_window = TableWindow()
        self.ui.connect_button.clicked.connect(self.show_connection_dialog)
        self.ui.disconnect_button.clicked.connect(self.disconnect_db)
        self.ui.manage_button.clicked.connect(self.show_table_window)

    def show_connection_dialog(self):
        connect_window = ConnectDialog()
        connect_window.exec_()
        if db.is_connected:
            self.ui.label.setText(f'Connected to {db.database_name}')

    def show_table_window(self):
        if not db.is_connected:
            QMessageBox.warning(self, 'Error', 'Not connected')
        else:
            self.table_window.show()

    def disconnect_db(self):
        if not db.is_connected:
            QMessageBox.warning(self, 'Error', 'Not connected')
        elif db.disconnect():
            self.ui.label.setText('Not connected')
        else:
            QMessageBox.warning(self, 'Error', 'Not disconnected')