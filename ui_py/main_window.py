# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'main.ui'
#
# Created by: PyQt5 UI code generator 5.15.2
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(332, 265)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.connect_button = QtWidgets.QPushButton(self.centralwidget)
        self.connect_button.setGeometry(QtCore.QRect(120, 80, 75, 23))
        self.connect_button.setObjectName("connect_button")
        self.manage_button = QtWidgets.QPushButton(self.centralwidget)
        self.manage_button.setGeometry(QtCore.QRect(120, 160, 75, 23))
        self.manage_button.setObjectName("manage_button")
        self.exit_button = QtWidgets.QPushButton(self.centralwidget)
        self.exit_button.setGeometry(QtCore.QRect(120, 210, 75, 23))
        self.exit_button.setObjectName("exit_button")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setEnabled(True)
        self.label.setGeometry(QtCore.QRect(130, 21, 171, 16))
        self.label.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.label.setObjectName("label")
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setGeometry(QtCore.QRect(30, 20, 91, 16))
        self.label_2.setObjectName("label_2")
        self.disconnect_button = QtWidgets.QPushButton(self.centralwidget)
        self.disconnect_button.setGeometry(QtCore.QRect(120, 120, 75, 23))
        self.disconnect_button.setObjectName("disconnect_button")
        MainWindow.setCentralWidget(self.centralwidget)
        self.action11 = QtWidgets.QAction(MainWindow)
        self.action11.setObjectName("action11")
        self.action22 = QtWidgets.QAction(MainWindow)
        self.action22.setObjectName("action22")
        self.action33 = QtWidgets.QAction(MainWindow)
        self.action33.setObjectName("action33")

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.connect_button.setText(_translate("MainWindow", "Connect"))
        self.manage_button.setText(_translate("MainWindow", "Managing"))
        self.exit_button.setText(_translate("MainWindow", "Exit"))
        self.label.setText(_translate("MainWindow", "Not connected"))
        self.label_2.setText(_translate("MainWindow", "Connection status"))
        self.disconnect_button.setText(_translate("MainWindow", "Disconnect"))
        self.action11.setText(_translate("MainWindow", "11"))
        self.action22.setText(_translate("MainWindow", "22"))
        self.action33.setText(_translate("MainWindow", "33"))
