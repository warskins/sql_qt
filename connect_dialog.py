from PyQt5.QtWidgets import QDialog, QMainWindow, QMessageBox

from config import get_config, createConfig
from db import db
from ui_py.dialog import Ui_Dialog


class ConnectDialog(QDialog, QMainWindow):
    def __init__(self):
        super(ConnectDialog, self).__init__()
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)
        self.setFixedSize(self.size())
        self.ui.connect_button.clicked.connect(self.connect)

        settings = get_config()
        if settings is not None:
            self.ui.input_host.setText(settings.get('Settings', 'host'))
            self.ui.input_user.setText(settings.get('Settings', 'user'))
            self.ui.input_password.setText(settings.get("Settings", "password"))
            self.ui.input_db.setText(settings.get("Settings", "database"))

    def connect(self):
        host = self.ui.input_host.text()
        user = self.ui.input_user.text()
        password = self.ui.input_password.text()
        database = self.ui.input_db.text()
        if db.connect(host, user, password, database):
            QMessageBox.information(self, 'Success', 'Connection established')
            if self.ui.save_settings_checkbox.isChecked():
                createConfig(host, user, password, database)
            self.close()
        else:
            QMessageBox.warning(self, 'Error', 'Not connected')